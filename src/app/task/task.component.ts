import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent {
  @Input() task = '';
  @Output() delete = new EventEmitter();
  @Output() taskChange = new EventEmitter<string>();
  icon: string ='https://d1nhio0ox7pgb.cloudfront.net/_img/v_collection_png/512x512/shadow/delete.png';

  onFocus(event: Event) {
    const target = <HTMLInputElement>event.target;
    target.style.borderBottom = '1px solid lightgreen';
    target.style.boxShadow = '0px 1px 1px 0px rgba(23, 192, 0, 0.16)';
  }

  onBlur(event: Event){
    const target = <HTMLInputElement>event.target;
    target.style.borderBottom = 'none';
    target.style.boxShadow = 'none';
  }

  onTaskInput(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.taskChange.emit(target.value);
  }

  onDelete(){
    this.delete.emit();
  }
}
