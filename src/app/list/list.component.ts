import { Component } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent  {
  task = '';
  tasks = [{task: 'Cook dinner'},
    {task: 'Learn English'},
    {task: 'Do homework 53'},];

  addTask(){
    this.tasks.push({task: this.task});
    console.log(this.tasks);
    this.task = '';
  }

  onDeleteTask(i:number){
    this.tasks.splice(i, 1);
    console.log(this.tasks);
  }

  changeTask(i:number, changed:string){
    this.tasks[i].task = changed;
  }


}
